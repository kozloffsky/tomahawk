<?php
/**
 * Created by PhpStorm.
 * User: Mykhaylo.Kozlovskyy
 * Date: 25-Mar-18
 * Time: 11:29 AM
 */

namespace Tomahawk;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class ApplicationContext
{

    private $container;

    public function __construct($root)
    {
        $this->container = new ContainerBuilder();
        $loader = new XmlFileLoader($this->container, new FileLocator($root.DIRECTORY_SEPARATOR.'config'));
        $loader->load('context.xml');
    }

    public function getContainer(){
        return $this->container;
    }
}