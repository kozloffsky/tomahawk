<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Mykhaylo.Kozlovskyy
 * Date: 25-Mar-18
 * Time: 3:42 PM
 */

namespace Tomahawk;


interface ApplicationInterface
{
    public function getContext();

    public function getRootPath();

    public function setBasePath(string $path);
}