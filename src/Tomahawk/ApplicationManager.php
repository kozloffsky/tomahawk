<?php declare(strict_types=1);

namespace Tomahawk;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Routing\RouteCollection;

class ApplicationManager
{
    private $applications;

    public function __construct(array $appInfo, ContainerBuilder $container)
    {
        $this->applications = [];
        var_dump($appInfo);
        foreach ($appInfo as $name => $info){
            if(key_exists('class', $info)){
                $app = new $info['class'];
                $app->setBasePath($info['uri']);
                $this->applications[$name] = $app;
                $container->merge($app->getContext()->getContainer());
            }
        }

        var_dump($this->applications);
    }

    public function getRoutes(){
        $routes = new RouteCollection();
        foreach ($this->applications as $application){
            $routes->addCollection($application->getRoutes());
        }
        return $routes;
    }

}