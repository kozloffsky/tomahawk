<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Mykhaylo.Kozlovskyy
 * Date: 24-Mar-18
 * Time: 6:33 PM
 */

namespace Tomahawk;


use React\EventLoop\LoopInterface;
use React\Http\Server;

class Kernel
{
    /**
     * @var \React\EventLoop\LoopInterface
     */
    private $loop;

    /**
     * @var Server
     */
    private $server;

    /**
     * @var \React\Socket\Server
     */
    private $socket;

    /**
     * @var ApplicationManager
     */
    private $applicationManager;




    public function __construct()
    {
        /*$this->loop = Factory::create();
        $factory = new HttpFoundationFactory();
        $psr7Factory = new DiactorosFactory();

        $this->server = new Server(function (ServerRequestInterface $request) use ($factory, $psr7Factory){
            $symfonyRequest = $factory->createRequest($request);
            $response = new Response($symfonyRequest->getPathInfo());
            return $psr7Factory->createResponse($response);
        });

        $this->socket = new \React\Socket\Server(8888, $this->loop);
        $this->server->listen($this->socket);

        $this->loop->run();*/
    }

    public function setLoop(LoopInterface $loop){
        $this->loop = $loop;
    }

    public function setSocket(\React\Socket\Server $server){
        $this->socket = $server;
    }

    public function setServer(Server $server){
        $this->server = $server;
    }

    public function setApplicationManager(ApplicationManager $applicationManager){
        $this->applicationManager = $applicationManager;
    }

    public function run(){
        $this->server->listen($this->socket);
        $this->loop->run();
    }

}