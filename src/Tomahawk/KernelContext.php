<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Mykhaylo.Kozlovskyy
 * Date: 24-Mar-18
 * Time: 8:43 PM
 */

namespace Tomahawk;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class KernelContext
{
    private $container;

    public function __construct($root)
    {
        $this->container = new ContainerBuilder();
        $loader = new XmlFileLoader($this->container, new FileLocator($root.DIRECTORY_SEPARATOR.'config'));
        $loader->load('context.xml');
    }


    private function run(){
        $this->container->get('tomahawk.kernel')->run();
    }

    public static function start($root){
        $context = new KernelContext($root);
        $context->run();
    }
}