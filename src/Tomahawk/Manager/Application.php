<?php
/**
 * Created by PhpStorm.
 * User: Mykhaylo.Kozlovskyy
 * Date: 25-Mar-18
 * Time: 3:41 PM
 */

namespace Tomahawk\Manager;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Tomahawk\ApplicationContext;
use Tomahawk\ApplicationInterface;

class Application implements ApplicationInterface
{
    private $basePath;

    private $context;

    public function __construct()
    {
        $this->context = new ApplicationContext($this->getRootPath());
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getRootPath()
    {
        return realpath(__DIR__);
    }

    public function setBasePath(string $path)
    {
        $this->basePath = $path;
    }


    public function getRoutes(){
        $fileLocator = new FileLocator(array($this->getRootPath().DIRECTORY_SEPARATOR.'config'));
        $loader = new YamlFileLoader($fileLocator);
        $routes = $loader->load('routes.yaml');
        $routes->addPrefix($this->basePath);
        return $routes;
    }
}