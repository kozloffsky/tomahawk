<?php
/**
 * Created by PhpStorm.
 * User: Mykhaylo.Kozlovskyy
 * Date: 24-Mar-18
 * Time: 10:00 PM
 */

namespace Tomahawk;


use Psr\Http\Message\ServerRequestInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

class RequestHandler
{
    private $factory;
    private $psrFactory;

    private $eventDispatcher;
    private $controllerResolver;
    private $argumentResolver;
    /**
     * @var ApplicationManager
     */
    private $applicationManager;

    public function __construct()
    {
        $this->factory = new HttpFoundationFactory();
        $this->psrFactory = new DiactorosFactory();

    }

    public function __invoke(ServerRequestInterface $request)
    {
        $symfonyRequest = $this->factory->createRequest($request);
        try {
            $requestContext = new RequestContext('/');
            $routes = $this->applicationManager->getRoutes();
            $matcher = new UrlMatcher($routes, $requestContext);

            $match = $matcher->match($symfonyRequest->getPathInfo());
            $symfonyRequest->attributes->add($match);
            $kernel = new HttpKernel($this->eventDispatcher,
                $this->controllerResolver,
                new RequestStack(),
                $this->argumentResolver);

            $response = $kernel->handle($symfonyRequest);
        }catch (NotFoundHttpException $e){
            $response = new Response('Not Found',404);
        }catch (\Exception $e){
            $response = new Response($e->getMessage());
            var_dump($e);
        }
        return $this->psrFactory->createResponse($response);
    }

    /**
     * @return mixed
     */
    public function getEventDispatcher()
    {
        return $this->eventDispatcher;
    }

    /**
     * @param mixed $eventDispatcher
     */
    public function setEventDispatcher($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @return mixed
     */
    public function getControllerResolver()
    {
        return $this->controllerResolver;
    }

    /**
     * @param mixed $controllerResolver
     */
    public function setControllerResolver($controllerResolver)
    {
        $this->controllerResolver = $controllerResolver;
    }

    /**
     * @return mixed
     */
    public function getArgumentResolver()
    {
        return $this->argumentResolver;
    }

    /**
     * @param mixed $argumentResolver
     */
    public function setArgumentResolver($argumentResolver)
    {
        $this->argumentResolver = $argumentResolver;
    }

    /**
     * @param mixed $applicationManager
     */
    public function setApplicationManager($applicationManager)
    {
        $this->applicationManager = $applicationManager;
    }




}